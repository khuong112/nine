const chai = require('chai');
const nine = require("../nine-test");

chai.should();

describe('Test: Extract nine programs', () => {
  
  it('Correct number of programs', async () => {
    // arrange
    var body = require("./request.json");
    var event = {
      body: JSON.stringify(body)
    }

    // act
    var response = await nine.handler(event);
    
    // assert
    var result = JSON.parse(response.body);
    
    result.response.length.should.equal(7);
  });

  it('Invalid JSON', async () => {
    // arrange
    var event = {
      body: "This is invalid JSON format"
    }

    // act
    var response = await nine.handler(event);
    var responseBody = JSON.parse(response.body);
    
    // assert
    responseBody.error.should.equal("Could not decode request: JSON parsing failed");
    response.statusCode.should.equal(400);
  });
});

