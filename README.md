# README #

### Nine technical assessment ###
* How to run tests

  ```npm test```

* Deployment
  
  The function in file ```nine-test.js``` and deployed to AWS Lambda function and using API Gateway

### Questions ###
#### What do you think about the test? ####

    - Fastest implementation is using ASW Lambda & API Gateway
    - Or we can implement properly by a RestfulAPI service by using ExpressJS framework or .Net Core 

#### The biggest challenge ####
    - The challenge is to implement and run the tests locally

#### How long did it take to complete? ####
    - The main function take about 10 mins
    - The test function take about 20 mins

#### How did you choose which language/technology to use? ####
  
  I use AWS Lambda& API Gateway because of these reasons:
    - Simplest implimentation, only 1 function required
    - It's serverless, managed and scaled automatically by AWS
    - It's free!!! I'm using AWS free-tier