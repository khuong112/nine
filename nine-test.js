exports.handler = async (event) => {
    try{
        var request = JSON.parse(event.body);
        var result = request.payload.filter(x => x.drm && parseInt(x.episodeCount) > 0)
                            .map((x) => {
                                return {
                                    image: x.image? x.image.showImage : null,
                                    slug: x.slug,
                                    title: x.title
                                };
                            });
        
        return {
            statusCode: 200,
            body: JSON.stringify({
                response: result
            })
        }
    } catch(e) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                "error": "Could not decode request: JSON parsing failed"
            })
        };
    }
};